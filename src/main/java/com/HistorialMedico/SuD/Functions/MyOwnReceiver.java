package com.HistorialMedico.SuD.Functions;

import com.vaadin.ui.Upload;

import java.io.ByteArrayOutputStream;

public class MyOwnReceiver implements Upload.Receiver {
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    @Override
    public ByteArrayOutputStream receiveUpload(String filename, String mimeType) {
        return byteArrayOutputStream;
    }
    public ByteArrayOutputStream getByteArrayOutputStream(){
        return byteArrayOutputStream;
    }
}
