package com.HistorialMedico.SuD.Functions;

import com.vaadin.server.StreamResource;
import com.vaadin.ui.Image;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class Functions {
    public static Image convertToImage(final byte[] imageData)
    {
        StreamResource.StreamSource streamSource = new StreamResource.StreamSource() {
            public InputStream getStream()
            {
                return (imageData == null) ? null : new ByteArrayInputStream(
                        imageData);
            }
        };

        return new Image(
                null, new StreamResource(
                streamSource, "streamedSourceFromByteArray"));
    }
}
