package com.HistorialMedico.SuD.Repositories;

import com.HistorialMedico.SuD.Entities.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface UsersRepository extends JpaRepository<Users, Long> {
    Users findUsersByUserName(String userName);
}
