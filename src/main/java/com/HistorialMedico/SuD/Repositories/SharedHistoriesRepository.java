package com.HistorialMedico.SuD.Repositories;

import com.HistorialMedico.SuD.Entities.History;
import com.HistorialMedico.SuD.Entities.SharedHistories;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface SharedHistoriesRepository extends JpaRepository<SharedHistories, Integer> {
    List<SharedHistories> findSharedHistoriesByHistory(History history);
}
