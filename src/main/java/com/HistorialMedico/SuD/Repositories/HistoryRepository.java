package com.HistorialMedico.SuD.Repositories;

import com.HistorialMedico.SuD.Entities.History;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface HistoryRepository extends JpaRepository<History, Integer> {
}
