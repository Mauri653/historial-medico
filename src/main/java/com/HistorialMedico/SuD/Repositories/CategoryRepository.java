package com.HistorialMedico.SuD.Repositories;

import com.HistorialMedico.SuD.Entities.Category;
import com.HistorialMedico.SuD.Entities.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface CategoryRepository extends JpaRepository<Category,Integer> {
    List<Category> findByUser(Users user);
}
