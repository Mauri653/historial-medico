package com.HistorialMedico.SuD.Repositories;

import com.HistorialMedico.SuD.Entities.MedicalInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface MedicalInfoRepository extends JpaRepository<MedicalInfo, Integer> {
}
