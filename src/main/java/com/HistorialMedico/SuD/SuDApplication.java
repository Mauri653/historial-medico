package com.HistorialMedico.SuD;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SuDApplication {
	public static void main(String[] args) {
		SpringApplication.run(SuDApplication.class, args);
	}
}
