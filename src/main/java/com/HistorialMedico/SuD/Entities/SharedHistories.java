package com.HistorialMedico.SuD.Entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class SharedHistories {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "idHistory")
    private History history;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "idUser")
    private Users user;

    @NotNull
    private Boolean edit;

    public SharedHistories(){

    }

    public SharedHistories(Users user, History history, Boolean edit){
        this.user=user;
        this.history=history;
        this.edit=edit;
    }

    public Integer getId() {
        return id;
    }

    public Boolean getEdit() {
        return edit;
    }

    public void setEdit(Boolean edit) {
        this.edit = edit;
    }

    public History getHistory() {
        return history;
    }

    public void setHistory(History history) {
        this.history = history;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }
}
