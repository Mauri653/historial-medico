package com.HistorialMedico.SuD.Entities;

import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Entity
public class History {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;



    @NotNull
    private String name;

    @NotNull
    private LocalDate userDate;

    @NotNull
    private LocalDate localDate;

    @Size(max = 3000000)
    @NotNull
    private String history;

    @Nullable
    private byte[] image;

    @ManyToOne
    @JoinColumn(name = "idUser")
    private Users user;

    @ManyToOne
    @JoinColumn(name ="idCategory")
    private Category category;

    @Nullable
    private String comment;

    public History(){
        localDate=LocalDate.now();
    }

    public History(String commentary,String name, String history, LocalDate userDate, byte[] image, Users user, Category category){
        this.name=name;
        this.comment =commentary;
        this.history=history;
        this.userDate=userDate;
        this.image=image;
        localDate=LocalDate.now();
        this.user=user;
        this.category=category;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHistory() {
        return history;
    }

    public void setHistory(String history) {
        this.history = history;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public void setLocalDate(LocalDate localDate) {
        this.localDate = localDate;
    }

    public LocalDate getUserDate() {
        return userDate;
    }

    public void setUserDate(LocalDate userDate) {
        this.userDate = userDate;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public byte[] getImage() {
        return image;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
