package com.HistorialMedico.SuD.UI;

import com.HistorialMedico.SuD.Entities.Users;
import com.HistorialMedico.SuD.Repositories.UsersRepository;
import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;

@SpringUI(path = "/confirm")
@Theme("HistoriesTheme")
public class ConfirmEmail extends UI {
    @Autowired
    private UsersRepository usersRepository;
    private VerticalLayout verticalLayout;
    @Autowired
    private MyMenuBar myMenuBar;

    @Override
    protected void init(VaadinRequest request) {
        enableUser();
        setupLayout();
        inserMiniBar();
        verticalLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        SetupAdvertisment();
        returnToHome();
    }

    private void enableUser() {
        Long id = Long.parseLong(getPage().getUriFragment());
        Users user = usersRepository.findById(id).get();
        user.setEnabled(true);
        usersRepository.save(user);
    }

    private void setupLayout() {
        verticalLayout = new VerticalLayout();
        verticalLayout.setSizeFull();
        verticalLayout.setMargin(false);
        setContent(verticalLayout);
    }

    private void inserMiniBar() {
        verticalLayout.addComponent(myMenuBar);
    }

    private void SetupAdvertisment() {
        Label advise = new Label("Se confirmo su correo de manera exitosa");
        advise.addStyleName(ValoTheme.LABEL_H1);
        verticalLayout.addComponent(advise);
    }

    private void returnToHome() {
        Button returnHome = new Button("Loguearse");
        returnHome.addClickListener(click ->{
            getPage().setLocation("/Login");
        });
        verticalLayout.addComponent(returnHome);
    }

}
