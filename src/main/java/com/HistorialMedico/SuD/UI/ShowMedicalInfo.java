package com.HistorialMedico.SuD.UI;


import com.HistorialMedico.SuD.Entities.MedicalInfo;
import com.HistorialMedico.SuD.Entities.Users;
import com.HistorialMedico.SuD.Repositories.MedicalInfoRepository;
import com.HistorialMedico.SuD.Repositories.UsersRepository;
import com.vaadin.annotations.Theme;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@SpringUI(path = "/ShowMedicalInfo")
@Theme("HistoriesTheme")
public class ShowMedicalInfo extends UI {
    @Autowired
    MedicalInfoRepository medicalInfoRepository;
    @Autowired
    private MyMenuBar myMenuBar;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private MedicalInfoForm medicalInfoForm;

    private MedicalInfo medInf;

    private VerticalLayout showMedroot;


    @Override
    protected void init(VaadinRequest vaadinRequest) {
        setupLayout();
        addMenuBar();
        addHeader();
        medicalInfo();
        editButton();
    }

    private void setupLayout(){
        showMedroot=new VerticalLayout();
        showMedroot.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        showMedroot.setMargin(false);
        showMedroot.setResponsive(true);
        setContent(showMedroot);
    }


    private void addMenuBar() {
        showMedroot.addComponent(myMenuBar);
    }
    private void addHeader() {
        Label header = new Label(" Informacion medica");
        header.addStyleName(ValoTheme.LABEL_H3);
        showMedroot.addComponent(header);
    }


    private void medicalInfo(){

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        Users user = usersRepository.findUsersByUserName(name);


        showMedroot.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);

        medInf = user.getMedicalInfo();
        Label date = new Label(medInf.getBirthDate().toString());
        date.setCaption("Fecha de nacimiento:");
        showMedroot.addComponent(date);

        Label gender = new Label(medInf.getGender());
        gender.setCaption("Genero:");
        showMedroot.addComponent(gender);

        Label bloodType = new Label(medInf.getBloodType());
        bloodType.setCaption("Grupo Sanguineo:");
        showMedroot.addComponent(bloodType);

        Label height = new Label(medInf.getHeight());
        height.setCaption("Altura(cm):");
        showMedroot.addComponent(height);

        Label weight = new Label(medInf.getWeight());
        weight.setCaption("Peso(kg):");
        showMedroot.addComponent(weight);

        Label allergies = new Label(medInf.getAllergies());
        allergies.setCaption("Alergias:");
        showMedroot.addComponent(allergies);

    }

    private void editButton(){
        Button edit = new Button();
        edit.setCaption("Editar");
        edit.addClickListener(click -> {

            getPage().setLocation("/MedicalInfo");

        });

        showMedroot.addComponent(edit);
    }





}
