package com.HistorialMedico.SuD.UI;


import com.HistorialMedico.SuD.Entities.Users;
import com.HistorialMedico.SuD.Repositories.UsersRepository;
import com.HistorialMedico.SuD.Services.MailService;
import com.vaadin.annotations.Theme;
import com.vaadin.data.Binder;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringUI(path = "/Register")
@Theme("HistoriesTheme")
public class AccountRegister extends UI {
    @Autowired
    private DaoAuthenticationProvider daoAuthenticationProvider;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    @Autowired
    private MyMenuBar myMenuBar;
    @Autowired
    private MailService mailService;
    private AbsoluteLayout registerRoot;

    @Override
    protected void init(VaadinRequest request) {
        setupLayout();
        addBarMenu();
        addCreateForm();

    }

    private void setupLayout() {
        registerRoot = new AbsoluteLayout();
        registerRoot.setSizeFull();
        registerRoot.setResponsive(true);
        setContent(registerRoot);
    }

    private void addBarMenu() {
        registerRoot.addComponent(myMenuBar, "top:0%; left:0%");
    }


    private void addCreateForm(){

        FormLayout layout = new FormLayout();
        layout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        layout.setMargin(true);
        layout.setWidth("60%");
        layout.setHeight("90%");
        layout.addStyleName("registerLayout");

        Label header = new Label("REGISTRARSE ");
        header.addStyleName(ValoTheme.LABEL_H3);

        TextField name = new TextField("Nombre");
        name.setSizeFull();

        TextField lastName = new TextField("Apellido: ");
        lastName.setSizeFull();

        TextField email = new TextField("Email");
        email.setSizeFull();

        PasswordField password = new PasswordField("Contraseña");
        password.setSizeFull();

        PasswordField Rpassword = new PasswordField("Confirmar Contraseña");
        Rpassword.setSizeFull();

        Binder<Users> binder = new Binder<>();
        binder.forField(name).withValidator(new StringLengthValidator("No puede estar vacio",1,30)).bind(Users::getFirstName, Users::setFirstName);
        binder.forField(lastName).withValidator(new StringLengthValidator("No puede estar vacio",1,30)).bind(Users::getLastName, Users::setLastName);
        binder.forField(email).withValidator(new EmailValidator("No es un email valido")).bind(Users::getUsername, Users::setUserName);
        binder.forField(password).withValidator(new StringLengthValidator("Debe ser de 6 caracteres como minimo", 6, null)).bind(Users::getPassword, Users::setPassword);

        layout.addComponent(header);
        layout.addComponent(name);
        layout.addComponent(lastName);
        layout.addComponent(email);
        layout.addComponent(password);
        layout.addComponent(Rpassword);

        Label validationStatus = new Label();
        binder.setStatusLabel(validationStatus);

        binder.setBean(new Users());

        registerRoot.addComponent(layout, "top:10%; left:25%;");

        Button registerButton = new Button("Registrate");
        registerButton.setSizeFull();
        registerButton.setEnabled(false);

        registerButton.addClickListener(click ->{
            if(password.getValue().equals(Rpassword.getValue())){
                if(usersRepository.findUsersByUserName(email.getValue())==null){
                    Users newUser = binder.getBean();
                    newUser.setPassword(passwordEncoder.encode(newUser.getPassword()));
                    usersRepository.save(newUser);
                    mailService.sendConfirmation(newUser);
                    Window confirm = new Window();
                    confirm.setWidth("50%");
                    confirm.setHeight("30%");
                    confirm.setModal(true);
                    VerticalLayout verticalLayout = new VerticalLayout();
                    verticalLayout.setSizeFull();
                    verticalLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
                    Label label = new Label("Por favor confirme su e-mail con el correo que le fue enviado");
                    verticalLayout.addComponent(label);
                    confirm.setContent(verticalLayout);
                    confirm.center();
                    addWindow(confirm);
                    confirm.addCloseListener(close ->{
                        if (newUser.getMedicalInfo()== null)
                        {
                            getPage().setLocation("/MedicalInfo");
                        }
                        else {
                            getPage().setLocation("/");
                        }
                    });
                }
                else {
                    Notification.show("Este email ya esta registrado");
                }
            }
            else {
                Notification.show("La contraseñas no son identicas");
            }
        });

        binder.addStatusChangeListener(event -> registerButton.setEnabled(binder.isValid()));

        Button loginBttn = new Button("Login");
        loginBttn.setSizeFull();

        loginBttn.addClickListener(click ->{
            getPage().setLocation("/Login");
        });

        layout.addComponent(registerButton);
        layout.addComponent(loginBttn);
    }

}
