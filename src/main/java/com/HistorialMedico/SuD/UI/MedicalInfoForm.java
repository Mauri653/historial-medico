package com.HistorialMedico.SuD.UI;

import com.HistorialMedico.SuD.Entities.History;
import com.HistorialMedico.SuD.Entities.MedicalInfo;
import com.HistorialMedico.SuD.Entities.Users;
import com.HistorialMedico.SuD.Repositories.MedicalInfoRepository;
import com.HistorialMedico.SuD.Repositories.UsersRepository;
import com.vaadin.annotations.Theme;
import com.vaadin.data.Binder;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@SpringUI(path = "/MedicalInfo")
@Theme("HistoriesTheme")
public class MedicalInfoForm extends UI {
    @Autowired
    MedicalInfoRepository medicalInfoRepository;
    @Autowired
    private MyMenuBar myMenuBar;
    @Autowired
    private UsersRepository usersRepository;

    private MedicalInfo medInf;

    private VerticalLayout medicalRoot;


    @Override
    protected void init(VaadinRequest request) {
        setupLayout();
        addMenuBar();
        addHeader();
        form();
    }



    private  void  setupLayout(){
        medicalRoot=new VerticalLayout();
        medicalRoot.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        medicalRoot.setMargin(false);
        medicalRoot.setResponsive(true);
        setContent(medicalRoot);

    }
    private void addMenuBar() {
        medicalRoot.addComponent(myMenuBar);
    }

    private void addHeader() {
        Label header = new Label(" Informacion medica");
        header.addStyleName(ValoTheme.LABEL_H3);
        medicalRoot.addComponent(header);
    }



    private void form(){
        medInf = new MedicalInfo();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        Users user = usersRepository.findUsersByUserName(name);

        DateField birthdate=new DateField("Fecha de nacimiento");
        TextField weight= new TextField("Peso (en kg)");
        TextField height= new TextField("Altura (en cm)");

        NativeSelect<String> bloodType = new NativeSelect<>("Grupo Sanguineo");
        bloodType.setItems("O+","O-","A+","A-","B+","B-","AB+","AB-");

        RadioButtonGroup<String> gender = new RadioButtonGroup<>("Genero");
        gender.setItems("Femenino", "Masculino", "Otro");

        TextArea allergies= new TextArea("Alergias");

        Binder<MedicalInfo> binder= new Binder<>();

        binder.setBean(medInf);

        binder.forField(birthdate)
                .bind(MedicalInfo::getBirthDate, MedicalInfo::setBirthDate);
        binder.forField(weight)
                .bind(MedicalInfo::getWeight, MedicalInfo::setWeight);

        binder.forField(height)
                .bind(MedicalInfo::getHeight, MedicalInfo::setHeight);
        binder.forField(bloodType)
                .bind(MedicalInfo::getBloodType, MedicalInfo::setBloodType);
        binder.forField(gender)
                .bind(MedicalInfo::getGender, MedicalInfo::setGender);
        binder.forField(allergies)
                .bind(MedicalInfo::getAllergies, MedicalInfo::setAllergies);

        Button saveButton = new Button("guardar");
        saveButton.addClickListener(click->{
            MedicalInfo newMedicalInfo = binder.getBean();


            medInf.setUser(user);
            medicalInfoRepository.save(newMedicalInfo);

            getPage().setLocation("/");
        });

        medicalRoot.addComponent(birthdate);
        medicalRoot.addComponent(gender);
        medicalRoot.addComponent(weight);
        medicalRoot.addComponent(height);
        medicalRoot.addComponent(bloodType);
        medicalRoot.addComponent(allergies);
        medicalRoot.addComponent(saveButton);


    }



}
