package com.HistorialMedico.SuD.UI;

import com.HistorialMedico.SuD.Entities.Category;
import com.HistorialMedico.SuD.Entities.History;
import com.HistorialMedico.SuD.Entities.SharedHistories;
import com.HistorialMedico.SuD.Entities.Users;
import com.HistorialMedico.SuD.Repositories.CategoryRepository;
import com.HistorialMedico.SuD.Repositories.HistoryRepository;
import com.HistorialMedico.SuD.Repositories.UsersRepository;
import com.vaadin.annotations.Theme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.List;
import java.util.stream.Collectors;

@SpringUI(path = "/Histories")
@Theme("HistoriesTheme")
public class Histories extends UI {

    @Autowired
    private HistoryRepository historyRepository;
    @Autowired
    private InsertWindow insertWindow;
    @Autowired
    private MyMenuBar myMenuBar;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private CategoryRepository categoryRepository;

    private Grid<History> historyGrid = new Grid<>();
    private Grid<SharedHistories> sharedHistoriesGrid = new Grid<>();
    private VerticalLayout root;
    private byte[] image;
    private ComboBox<Category> comboBox;

    @Override
    protected void init(VaadinRequest request) {
        setupLayout();
        addMenuBar();
        addHeader();
        addCreateButton();
        addFilterBox();
        addGrid();
        addSharedGrid();
    }

    private void setupLayout() {
        if(getActualUser().getMedicalInfo() == null){
            getUI().getPage().setLocation("/MedicalInfo");
        }
        else{
            root = new VerticalLayout();
            root.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
            root.setMargin(false);
            root.setResponsive(true);
            setContent(root);
        }
    }

    private void addMenuBar() {
        root.addComponent(myMenuBar);
    }

    private void addHeader() {
        Label header = new Label("Historial Medico");
        header.addStyleName(ValoTheme.LABEL_H1);
        root.addComponent(header);
    }

    private void addGrid() {
        root.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        historyGrid.setItems(getActualUser().getHistories());
        historyGrid.addColumn(History::getName).setCaption("Titulo");
        historyGrid.addColumn(history -> history.getCategory().getName()).setCaption("Categoria");
        historyGrid.addColumn(History::getUserDate).setCaption("Fecha");
        historyGrid.setResponsive(true);
        historyGrid.setWidth("80%");
        historyGrid.addItemClickListener(e -> Page.getCurrent().setLocation("Histories/show#" + e.getItem().getId().toString()));
        root.addComponent(historyGrid);
    }

    private void addSharedGrid(){
        root.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        sharedHistoriesGrid.setItems(getActualUser().getSharedHistories());
        sharedHistoriesGrid.addColumn(sharedHistories -> sharedHistories.getHistory().getName()).setCaption("Titulo");
        sharedHistoriesGrid.addColumn(sharedHistories -> sharedHistories.getHistory().getUser().getFirstName()).setCaption("Propietario");
        sharedHistoriesGrid.addColumn(sharedHistories -> sharedHistories.getHistory().getUserDate()).setCaption("Fecha");
        sharedHistoriesGrid.setResponsive(true);
        sharedHistoriesGrid.setWidth("80%");
        sharedHistoriesGrid.addItemClickListener(e -> Page.getCurrent().setLocation("Histories/show#S" + e.getItem().getId()));
    }

    private void addCreateButton() {
        Button Create = new Button();
        Create.setResponsive(true);
        Create.addClickListener(click -> {
            CreateWindow();
        });
        Create.setCaption("Ingresar historia");
        Create.setIcon(VaadinIcons.PLUS);
        Create.addStyleName("positionCreate");
        root.setDefaultComponentAlignment(Alignment.MIDDLE_RIGHT);
        root.addComponent(Create);
    }

    private void addFilterBox() {
        root.setDefaultComponentAlignment(Alignment.MIDDLE_LEFT);
        List<Category> categories= getActualUser().getCategories();
        categories.add(new Category("Historias compartidas", getActualUser()));
        comboBox = new ComboBox<>();
        comboBox.setWidth("20%");
        comboBox.setEmptySelectionCaption("Mis historias");
        comboBox.addStyleName("positionBox");
        comboBox.setItems(categories);
        comboBox.setItemCaptionGenerator(Category::getName);
        comboBox.addValueChangeListener(event -> {
            if(comboBox.getValue()!= null){
                if(comboBox.getValue().getName() != "Historias compartidas"){
                    root.removeComponent(sharedHistoriesGrid);
                    historyGrid.setItems(comboBox.getValue().getHistories().stream().filter(history -> history.getUser().getId()==getActualUser().getId()).collect(Collectors.toList()));
                    root.addComponent(historyGrid);
                }
                else{
                    root.removeComponent(historyGrid);
                    root.addComponent(sharedHistoriesGrid);
                }
            }
            else{
                root.removeComponent(sharedHistoriesGrid);
                historyGrid.setItems(getActualUser().getHistories());
                root.addComponent(historyGrid);
            }
        });
        root.addComponent(comboBox);
    }

    private void CreateWindow() {
        insertWindow.center();
        insertWindow.initNew();
        insertWindow.setHeight("95%");
        insertWindow.setWidth("60%");
        insertWindow.setResizable(false);
        addWindow(insertWindow);
        insertWindow.addCloseListener(close -> {
            historyGrid.setItems(getActualUser().getHistories());
            List<Category> categories= getActualUser().getCategories();
            categories.add(new Category("Historias compartidas", getActualUser()));
            comboBox.setItems(categories);
        });
    }

    private Users getActualUser(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        Users user = usersRepository.findUsersByUserName(name);
        return user;
    }
}
