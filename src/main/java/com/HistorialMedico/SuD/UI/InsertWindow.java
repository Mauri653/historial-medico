package com.HistorialMedico.SuD.UI;

import com.HistorialMedico.SuD.Entities.Category;
import com.HistorialMedico.SuD.Entities.History;
import com.HistorialMedico.SuD.Entities.Users;
import com.HistorialMedico.SuD.Functions.MyOwnReceiver;
import com.HistorialMedico.SuD.Repositories.CategoryRepository;
import com.HistorialMedico.SuD.Repositories.HistoryRepository;
import com.HistorialMedico.SuD.Repositories.UsersRepository;
import com.vaadin.data.Binder;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.StreamVariable;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.*;
import com.vaadin.ui.dnd.FileDropTarget;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.List;

@UIScope
@SpringComponent
public class InsertWindow extends Window {

    @Autowired
    private HistoryRepository historyRepository;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private CategoryRepository categoryRepository;

    private Binder<History> HistoryBinder;
    private History h;
    private AbsoluteLayout insert;
    private DateField userDate;
    private TextField comment;
    private TextField name;
    private byte[] imageSaved;
    private RichTextArea history;
    private ComboBox<Category> category;

    void initNew(){
        this.setModal(true);
        initHistory();
        setupLayout();
        addComboBox();
        actualDate();
        title();
        dragAndDrop();
        historyData();
        insertComment();
        buttonSave();
    }

    void initModify(History hist) {
        this.setModal(true);
        initHistory(hist);
        setupLayout();
        addComboBox();
        actualDate();
        title();
        editImage(hist);
        historyData();
        insertComment();
        buttonSave();
        HistoryBinder = new Binder<>(History.class);
        HistoryBinder.bindInstanceFields(this);
        HistoryBinder.setBean(hist);
    }

    private void initHistory() {
        h = new History();
    }

    private void initHistory(History h) {
        this.h = h;
    }

    private void setupLayout() {
        insert = new AbsoluteLayout();
        insert.setSizeFull();
        setContent(insert);
    }

    private void addComboBox() {
        category = new ComboBox<>();
        category.setCaption("Categoria");
        category.setItems(searchCategories());
        category.setItemCaptionGenerator(Category::getName);
        category.addValueChangeListener(event -> {
            if(category.getValue().getName().equals("CREAR NUEVO")){
                Window createCategory = new Window("Crear categoria");
                createCategory.setWidth("30%");
                createCategory.center();
                FormLayout formLayout = new FormLayout();
                formLayout.setMargin(true);
                formLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
                TextField textField = new TextField("Nombre:");
                textField.setSizeFull();;
                Button button = new Button("Guardar");
                button.setSizeFull();
                button.addClickListener(click -> {
                    Category n = new Category(textField.getValue(), getActualUser());
                    categoryRepository.save(n);
                    createCategory.close();
                    category.setItems(searchCategories());
                    category.setSelectedItem(n);
                });
                formLayout.addComponent(textField);
                formLayout.addComponent(button);
                createCategory.setContent(formLayout);
                getUI().addWindow(createCategory);
            }
        });
        insert.addComponent(category, "top: 5%; left: 5%;");
    }

    private List<Category> searchCategories() {
        Category cat = new Category("CREAR NUEVO", getActualUser());
        List<Category> categories = getActualUser().getCategories();
        categories.add(cat);
        return categories;
    }

    private void actualDate() {
        userDate = new DateField("Fecha");
        insert.addComponent(userDate, "top: 14%; left: 5%;");
    }

    private void title() {
        name = new TextField("Titulo");
        insert.addComponent(name, "top: 23%; left: 5%;");
    }

    private void insertComment() {
        comment = new TextField("Comentario");
        comment.setWidth("80%");
        insert.addComponent(comment, "top: 93%; left: 5%;");
    }

    private void editImage(History history) {
        if(history.getImage()!=null){
            Button delete = new Button("Eliminar imagen");
            Button edit = new Button("Cambiar imagen");
            insert.addComponent(edit, "top: 15%; right: 5%;");
            insert.addComponent(delete, "top: 25%; right: 5%;");
            edit.addClickListener(click ->{
                dragAndDrop();
                insert.removeComponent(edit);
                insert.removeComponent(delete);
            });
            delete.addClickListener(click ->{
                imageSaved = null;
                h.setImage(imageSaved);
                dragAndDrop();
                insert.removeComponent(edit);
                insert.removeComponent(delete);
            });
        }
        else{
            dragAndDrop();
        }
    }

    private void dragAndDrop() {
        HorizontalLayout dragAndDrop = DragAndDrop();
        insert.addComponent(dragAndDrop, "top: 20%; right: 5%;");
    }

    private HorizontalLayout DragAndDrop (){
        final Label infoLabel = new Label();
        infoLabel.setContentMode(ContentMode.HTML);
        infoLabel.setValue(VaadinIcons.UPLOAD.getHtml() + " Arrastra una imagen (Opcional)");

        final Label checkLabel = new Label();
        checkLabel.setContentMode(ContentMode.HTML);
        checkLabel.setValue(VaadinIcons.CHECK.getHtml() + " !  Se guardo correctamente   !");
        checkLabel.setVisible(false);

        final Label errorLabel = new Label();
        errorLabel.setValue("!!!     Ups algo fallo     !!!");
        errorLabel.setVisible(false);

        Upload upload = new Upload();
        MyOwnReceiver myOwnReceiver = new MyOwnReceiver();
        upload.setReceiver(myOwnReceiver);
        upload.addFinishedListener(finishedEvent -> {
            infoLabel.setVisible(false);
            saveFile(myOwnReceiver.getByteArrayOutputStream());
            checkLabel.setVisible(true);
        });

        HorizontalLayout dropR = new HorizontalLayout(upload,infoLabel,checkLabel);
        dropR.setComponentAlignment(infoLabel, Alignment.MIDDLE_LEFT);
        dropR.setComponentAlignment(checkLabel, Alignment.MIDDLE_LEFT);
        dropR.addStyleName("dropZone");
        dropR.setSizeUndefined();

        ProgressBar progress = new ProgressBar();
        progress.setIndeterminate(true);
        progress.setVisible(false);
        dropR.addComponent(progress);

        new FileDropTarget<>(dropR, fileDropEvent -> {
            final int fileSizeLimit = 2 * 1024 * 1024; // 2MB

            fileDropEvent.getFiles().forEach(html5File -> {
                final String fileName = html5File.getFileName();

                if (html5File.getFileSize() > fileSizeLimit) {
                    Notification.show(
                            "File rejected. Max 2MB files are accepted by Sampler",
                            Notification.Type.WARNING_MESSAGE);
                } else {
                    final ByteArrayOutputStream bas = new ByteArrayOutputStream();
                    final StreamVariable streamVariable = new StreamVariable() {

                        @Override
                        public OutputStream getOutputStream() {
                            return bas;
                        }

                        @Override
                        public boolean listenProgress() {
                            return false;
                        }

                        @Override
                        public void onProgress(
                                final StreamingProgressEvent event) {

                        }

                        @Override
                        public void streamingStarted(
                                final StreamingStartEvent event) {

                        }

                        @Override
                        public void streamingFinished(
                                final StreamingEndEvent event) {
                            progress.setVisible(false);
                            checkLabel.setVisible(true);
                            saveFile(bas);
                        }

                        @Override
                        public void streamingFailed(
                                final StreamingErrorEvent event) {
                            progress.setVisible(false);
                            errorLabel.setVisible(true);
                        }

                        @Override
                        public boolean isInterrupted() {
                            return false;
                        }
                    };
                    html5File.setStreamVariable(streamVariable);
                    infoLabel.setVisible(false);
                    progress.setVisible(true);
                    checkLabel.setVisible(false);
                }
            });
        });
        return dropR;
    }

    private void saveFile(final ByteArrayOutputStream bas) {
        imageSaved = bas.toByteArray();
        h.setImage(imageSaved);
    }

    private void historyData() {
        history = new RichTextArea();
        history.setCaption("Historia");
        history.setWidth("95%");
        history.setHeight("85%");
        insert.addComponent(history,"top: 33%; left: 5%;");
    }

    private void buttonSave() {
        Button Save = new Button("Guardar");
        insert.addComponent(Save,"bottom: 1%; right: 5%;");
        Save.addClickListener(click ->{
            if(category.getValue() != null){
                if(name.getValue() != ""){
                    if(userDate.getValue() != null){
                        if(history.getValue() != ""){
                            if(h.getUser()==null){
                                if(getActualUser().getRole().equals("DOCTOR")){
                                    Window pacient = new Window("Donde guardo");
                                    pacient.setModal(true);
                                    pacient.setResizable(false);
                                    pacient.setWidth("30%");
                                    pacient.setHeight("20%");
                                    HorizontalLayout buttons = new HorizontalLayout();
                                    buttons.setSizeFull();
                                    buttons.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
                                    Button me = new Button("Mi historial");
                                    me.addClickListener(event -> {
                                        pacient.close();
                                        h.setUser(getActualUser());
                                        h.setCategory(category.getValue());
                                        save();
                                    });
                                    Button other = new Button("Historial paciente");
                                    other.addClickListener(event -> {
                                        pacient.close();
                                        Window shareWindow = new Window();
                                        shareWindow.center();
                                        shareWindow.setModal(true);
                                        shareWindow.setWidth("30%");
                                        shareWindow.setHeight("30%");
                                        FormLayout content = new FormLayout();
                                        content.setMargin(true);
                                        content.setSizeFull();
                                        Label title = new Label("Introduzca su email");
                                        title.setStyleName(ValoTheme.LABEL_H3);
                                        content.addComponent(title);
                                        TextField email = new TextField("E-mail");
                                        email.setSizeFull();
                                        content.addComponent(email);
                                        Button shareNow = new Button("Guardar");
                                        shareNow.setSizeFull();
                                        shareNow.addClickListener(save -> {
                                            Users user = usersRepository.findUsersByUserName(email.getValue());
                                            if(user!=null){
                                                h.setUser(user);
                                                categoryRepository.findByUser(user).forEach(category1 -> {
                                                    if(category.getValue().getName().equals(category1.getName())){
                                                        h.setCategory(category1);
                                                        shareWindow.close();
                                                        save();
                                                    }
                                                });
                                                if(h.getCategory() == null) {
                                                    Category cat = category.getValue();
                                                    cat.setId(null);
                                                    cat.setUser(user);
                                                    categoryRepository.save(cat);
                                                    h.setCategory(cat);
                                                    shareWindow.close();
                                                    save();
                                                }
                                            }
                                            else{
                                                shareWindow.close();
                                                Notification.show("El paciente no esta registrado en la aplicacion");
                                            }
                                        });
                                        content.addComponent(shareNow);
                                        getUI().addWindow(shareWindow);
                                        shareWindow.setContent(content);
                                    });
                                    buttons.addComponent(me);
                                    buttons.addComponent(other);
                                    pacient.setContent(buttons);
                                    getUI().addWindow(pacient);
                                }
                                else{
                                    h.setUser(getActualUser());
                                    h.setCategory(category.getValue());
                                    save();
                                }
                            }
                            else {
                                h.setUser(getActualUser());
                                h.setCategory(category.getValue());
                                save();
                            }
                        }
                        else
                        {
                            Notification.show("No puedes dejar la historia vacia");
                        }
                    }
                    else{
                        Notification.show("No se puede dejar la fecha vacia");
                    }
                }
                else{
                    Notification.show("Necesita un titulo");
                }
            }
            else{
                Notification.show("No se puede introducir una categoria vacia");
            }
        });
    }

    private void save(){
        h.setComment(comment.getValue());
        h.setName(name.getValue());
        h.setHistory(history.getValue());
        h.setUserDate(userDate.getValue());
        historyRepository.save(h);
        name = null;
        history = null;
        userDate = null;
        imageSaved = null;
        this.close();
        Notification.show("Se Guardo correctamente");
    }

    private Users getActualUser(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        Users user = usersRepository.findUsersByUserName(name);
        return user;
    }
}
