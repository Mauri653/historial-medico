package com.HistorialMedico.SuD.UI;


import com.HistorialMedico.SuD.Entities.Users;
import com.HistorialMedico.SuD.Repositories.MedicalInfoRepository;
import com.HistorialMedico.SuD.Repositories.UsersRepository;
import com.vaadin.annotations.Theme;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@SpringUI(path = "/Profile")
@Theme("HistoriesTheme")
public class Profile extends UI {

    @Autowired
    private MedicalInfoRepository medicalInfoRepository;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private MyMenuBar myMenuBar;

    private Users user;
    private VerticalLayout profileRoot;
    private FormLayout info;
    private Label role;


    @Override
    protected void init(VaadinRequest vaadinRequest) {

        setupLayout();
        addMenuBar();
        setupInfoLayout();
        addHeader();
        profileInfo();
        viewMedicalInfo();
        deleteAccount();
        amIaDoctor();
        addInfoLayout();
    }

    private void setupLayout() {
        profileRoot = new VerticalLayout();
        profileRoot.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        profileRoot.setMargin(false);
        profileRoot.setResponsive(true);
        setContent(profileRoot);
    }

    private void addMenuBar() {
        profileRoot.addComponent(myMenuBar);
    }

    private void setupInfoLayout() {
        profileRoot.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        info = new FormLayout();
        info.setWidth("40%");
        info.setStyleName("registerLayout");
    }



    private void addHeader(){
        Label header = new Label("Mi perfil");
        header.addStyleName(ValoTheme.LABEL_H1);
        info.addComponent(header);
    }

    private void profileInfo(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        Users user = usersRepository.findUsersByUserName(name);


        Label firstName = new Label("Nombre: " + user.getFirstName());
        firstName.addStyleName(ValoTheme.LABEL_BOLD);
        Label lastName = new Label("Apellido: " + user.getLastName());
        lastName.addStyleName(ValoTheme.LABEL_BOLD);
        role = new Label("Rol: " + user.getRole());
        role.addStyleName(ValoTheme.LABEL_BOLD);


        info.addComponent(firstName);

        info.addComponent(lastName);

        info.addComponent(role);
    }

    private void viewMedicalInfo(){

        Button medInfo= new Button("Informacion Medica");
         medInfo.addClickListener(click -> {
             getPage().setLocation("/ShowMedicalInfo");
         });

         medInfo.setWidth("98%");
         info.addComponent(medInfo);

    }

    private  void deleteAccount(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        Users user = usersRepository.findUsersByUserName(name);
        Button delete= new Button("Eliminar cuenta");
        delete.addClickListener(click -> {
            Window verfication = new Window("Seguro que deseas eliminar?");
            verfication.setWidth("30%");
            verfication.setModal(true);
            verfication.setClosable(false);
            verfication.setResizable(false);
            verfication.setWidth("25%");
            verfication.setHeight("18%");
            HorizontalLayout buttons = new HorizontalLayout();
            buttons.setSizeFull();
            buttons.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
            buttons.setMargin(true);
            Button yes = new Button("SI");
            Button no = new Button("NO");
            buttons.addComponents(yes,no);
            verfication.setContent(buttons);
            yes.addClickListener(event -> {
                usersRepository.delete(user);
                Page.getCurrent().setLocation("/logout");
                verfication.close();
            });
            no.addClickListener(event -> verfication.close());
            verfication.center();
            addWindow(verfication);

        });

        delete.setWidth("98%");
        info.addComponent(delete);
    }

    private void amIaDoctor() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        Users user = usersRepository.findUsersByUserName(name);
        Button amI = new Button();
        if(user.getRole().equals("DOCTOR")){
            amI.setCaption("no soy doctor");
            amI.addClickListener(click ->{
                user.setRole("USUARIO SIMPLE");
                usersRepository.save(user);
                role.setValue("Rol: " + user.getRole());
                info.removeComponent(amI);
                amIaDoctor();
            });
        }
        else{
            amI.setCaption("soy doctor");
            amI.addClickListener(click ->{
                user.setRole("DOCTOR");
                usersRepository.save(user);
                role.setValue("Rol: " + user.getRole());
                info.removeComponent(amI);
                amIaDoctor();
            });
        }
        amI.setWidth("98%");
        info.addComponent(amI);
    }

    private void addInfoLayout() {
        profileRoot.addComponent(info);
    }

}
