package com.HistorialMedico.SuD.UI;

import com.HistorialMedico.SuD.Repositories.UsersRepository;
import com.vaadin.server.Page;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.annotation.PostConstruct;

@UIScope
@SpringComponent
public class MyMenuBar extends com.vaadin.ui.MenuBar {

    MenuItem menu;
    MenuItem account;
    @Autowired
    private UsersRepository usersRepository;

    @PostConstruct
    void init(){
        this.setWidth("100%");
        addMenu();
    }

    private void addMenu() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        if(name.equals("anonymousUser")){
            MenuItem home = this.addItem("HOME",click -> Page.getCurrent().setLocation("/"));
        }
        else{
            account = this.addItem(usersRepository.findUsersByUserName(name).getFirstName());
            menu = this.addItem("MENU");
            addItems();
        }
    }

    private void addItems() {
        MenuItem home = menu.addItem("Home", click -> {
            Page.getCurrent().setLocation("/");
        });
        MenuItem histories = menu.addItem("Historial", click -> {
            Page.getCurrent().setLocation("/Histories");
        });
        MenuItem profile = account.addItem("Mi Perfil", click ->{
           Page.getCurrent().setLocation("/Profile");
        });
        MenuItem logout = account.addItem("Salir", click -> {
            Page.getCurrent().setLocation("/logout");
        });
    }
}
