package com.HistorialMedico.SuD.UI;

import com.HistorialMedico.SuD.Repositories.UsersRepository;
import com.vaadin.annotations.Theme;
import com.vaadin.server.Page;
import com.vaadin.server.Resource;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@SpringUI(path = "/")
@Theme("HistoriesTheme")
public class Home extends UI {

    private VerticalLayout content;
    private CssLayout root;
    @Autowired
    private MyMenuBar myMenuBar;
    @Autowired
    private UsersRepository usersRepository;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        setContentLayout();
        addMenuBar();
        setupLayout();
        addHeader();
        setLogo();
        checkUser();
    }

    private void setContentLayout(){
        root = new CssLayout();
        root.setResponsive(true);
        root.setSizeFull();
        setContent(root);
    }

    private void addMenuBar() {
        root.addComponent(myMenuBar);
    }

    private void setupLayout() {
        content = new VerticalLayout();
        content.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        content.setResponsive(true);
        content.setMargin(false);
        content.setHeight("90%");
        root.addComponent(content);
    }


    private void addHeader() {
        Label header = new Label("Historial Medico Digital");
        header.addStyleName(ValoTheme.LABEL_H1);
        header.setResponsive(true);
        content.addComponent(header);
    }

    private void setLogo() {
        Resource res = new ThemeResource("../Images/LOGO.png");
        Image image = new Image(null, res);
        image.setHeight("100%");
        image.setResponsive(true);
        content.addComponent(image);
    }

    private void checkUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        if(name.equals("anonymousUser")){
            VerticalLayout buttons = new VerticalLayout();
            buttons.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
            loginButton(buttons);
            registerButton(buttons);
            buttons.setResponsive(true);
            buttons.setMargin(false);
            content.addComponent(buttons);
            buttons.setSizeFull();
        }
        else if(usersRepository.findUsersByUserName(name).getMedicalInfo() == null){
            getPage().setLocation("/MedicalInfo");

        }
        else{
            hitoriesButton();
        }
    }

    private void hitoriesButton() {
        Button historiesBttn = new Button("Historial Medico");
        historiesBttn.addClickListener(click ->{
            Page.getCurrent().setLocation("/Histories");
        });
        content.addComponent(historiesBttn);
    }

    private void loginButton(VerticalLayout buttons){
        Button loginButton = new Button("Login");
        loginButton.addClickListener(click ->{
            Page.getCurrent().setLocation("/Login");
        });
        loginButton.setResponsive(true);
        buttons.addComponent(loginButton);
    }

    private void registerButton(VerticalLayout buttons){
        Button registerButton = new Button("Sign-Up");
        registerButton.addClickListener(click ->{
            Page.getCurrent().setLocation("/Register");
        });
        registerButton.setResponsive(true);
        buttons.addComponent(registerButton);
    }

}
