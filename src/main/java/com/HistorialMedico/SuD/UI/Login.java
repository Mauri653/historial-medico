package com.HistorialMedico.SuD.UI;


import com.HistorialMedico.SuD.Repositories.UsersRepository;
import com.HistorialMedico.SuD.Services.MailService;
import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.w3c.dom.Text;

//Hola mundo
@SpringUI(path = "/Login")
@Theme("HistoriesTheme")
public class Login extends UI {

    @Autowired
    private DaoAuthenticationProvider daoAuthenticationProvider;
    @Autowired
    private MyMenuBar myMenuBar;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private MailService mailService;
    private AbsoluteLayout root;


    @Override
    protected void init(VaadinRequest request) {
        setupLayout();
        addMenuBar();
        addCreateForm();

    }

    private void setupLayout() {
        root = new AbsoluteLayout();
        root.setSizeFull();
        root.setResponsive(true);
        setContent(root);
    }

    private void addMenuBar() {
        root.addComponent(myMenuBar, "top:0%; left:0%");
    }

    private void addCreateForm(){

        FormLayout layout = new FormLayout();
        layout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        layout.setMargin(true);
        layout.setWidth("50%");
        layout.setHeight("70%");
        layout.addStyleName("registerLayout");

        Label header = new Label("LOGIN ");
        header.addStyleName(ValoTheme.LABEL_H3);

        TextField email = new TextField("E-mail");
        email.setSizeFull();
        PasswordField password = new PasswordField("Contraseña");
        password.setSizeFull();

        Button passwordRecover = new Button();
        passwordRecover.setCaption("Olvide mi contraseña");
        passwordRecover.addStyleName(ValoTheme.BUTTON_LINK);
        passwordRecover.addClickListener(click ->{
            Window recover = new Window();
            recover.setWidth("40%");
            recover.setCaption("Recuperacion de contraseña");
            recover.setResizable(false);
            FormLayout contentRecover = new FormLayout();
            contentRecover.setMargin(true);
            contentRecover.setWidth("100%");
            TextField emailR = new TextField("Introduzca su e-mail: ");
            emailR.setWidth("100%");
            contentRecover.addComponent(emailR);
            Button recoverPass = new Button("Recuperar contraseña");
            recoverPass.setWidth("100%");
            recoverPass.addClickListener(clickEvent -> {
                if(usersRepository.findUsersByUserName(emailR.getValue())!=null){
                    mailService.passwordHasForgotten(usersRepository.findUsersByUserName(emailR.getValue()));
                    recover.close();
                    Notification.show("Se le envio un correo de recuperacion de contraseña");
                }
                else{
                    Notification.show("El usuario no existe");
                }
            });
            contentRecover.addComponent(recoverPass);
            recover.setContent(contentRecover);
            addWindow(recover);
            recover.setModal(true);
            recover.center();
        });
        passwordRecover.setWidth("100%");


        Button loginBttn = new Button("Ingresar");
        loginBttn.setSizeFull();

        loginBttn.addClickListener(click ->{
            if(usersRepository.findUsersByUserName(email.getValue())!=null){
                if(usersRepository.findUsersByUserName(email.getValue()).getEnabled()){
                    try {
                        Authentication auth = new UsernamePasswordAuthenticationToken(email.getValue(),password.getValue());
                        Authentication authenticated = daoAuthenticationProvider.authenticate(auth);
                        SecurityContextHolder.getContext().setAuthentication(authenticated);
                        getPage().setLocation("/Histories");
                    } catch (Exception e){
                        Notification.show("La contraseña es incorrecta");
                    }
                }
                else{
                    Notification.show("No se confirmo el correo por favor revise su correo");
                }
            }
            else {
                Notification.show("El usuario no esta registrado");
            }
        });

        Button reggisterBttn = new Button("Registrarse");
        reggisterBttn.setSizeFull();

        reggisterBttn.addClickListener(click ->{
            getPage().setLocation("/Register");
        });

        layout.addComponent(header);
        layout.addComponent(email);
        layout.addComponent(password);
        layout.addComponent(passwordRecover);
        layout.addComponent(loginBttn);
        layout.addComponent(reggisterBttn);

        root.addComponent(layout,"top:25%; left:35%;");
    }
}
