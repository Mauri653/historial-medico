package com.HistorialMedico.SuD.UI;

import com.HistorialMedico.SuD.Entities.History;
import com.HistorialMedico.SuD.Functions.Functions;
import com.HistorialMedico.SuD.Repositories.HistoryRepository;
import com.HistorialMedico.SuD.Repositories.UsersRepository;
import com.HistorialMedico.SuD.Services.MailService;
import com.vaadin.annotations.Theme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import elemental.json.JsonArray;
import org.springframework.beans.factory.annotation.Autowired;

@SpringUI(path = "/shared")
@Theme("HistoriesTheme")
public class ShowSharedHistory extends UI {
    @Autowired
    private HistoryRepository historyRepository;
    @Autowired
    private InsertWindow insertWindow;
    @Autowired
    private MyMenuBar myMenuBar;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private MailService mailService;
    private AbsoluteLayout root;
    private History history;
    private RichTextArea data;

    @Override
    protected void init(VaadinRequest request) {
        setupLayout();
        addMenuBar();
        gettingHistory();
        addHeader();
        insertName();
        insertDate();
        contentHistory();
        buttonShow();
        buttonPrint();
    }

    private void setupLayout() {
        root = new AbsoluteLayout();
        root.setSizeFull();
        setContent(root);
    }

    private void addMenuBar() {
        root.addComponent(myMenuBar, "top:0%; left:0%");
    }

    private void gettingHistory() {
        Integer id = Integer.parseInt(getPage().getUriFragment());
        history = historyRepository.findById(id).get();
    }

    private void addHeader() {
        VerticalLayout title = new VerticalLayout();
        title.setWidth("100%");
        title.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        Label header = new Label(history.getName());
        header.addStyleName(ValoTheme.LABEL_H1);
        title.addComponent(header);
        root.addComponent(title, "top:5%; left:0%;");
    }

    private void insertName() {
        Label name = new Label(history.getUser().getFirstName()+" "+history.getUser().getLastName());
        name.setCaption("Nombre:");
        root.addComponent(name, "top: 30%; left:5%;");
    }

    private void insertDate() {
        Label date = new Label(history.getUserDate().toString());
        date.setCaption("Fecha:");
        root.addComponent(date, "top: 38%; left:5%;");
    }

    private void contentHistory() {
        data = new RichTextArea();
        data.setReadOnly(true);
        data.setValue(history.getHistory());
        data.setCaption("Historia");
        data.addStyleName("historyLabel");
        data.setSizeFull();
        root.addComponent(data, "top: 46%; left:5%");
    }

    private void buttonShow() {
        Button show = new Button();
        show.addClickListener(click -> {
            if(history.getImage()!=null){
                showWindow();
            }
            else{
                Notification.show("No se almaceno una imagen");
            }
        });
        show.setIcon(VaadinIcons.FILE_PICTURE);
        show.setCaption("Mostrar imagen");
        root.addComponent(show, "top: 46%; left:75%;");
    }

    private void showWindow(){
        final Window showImage = new Window("Imagen");
        showImage.setModal(true);
        showImage.center();
        showImage.setWidth("50%");
        Image image = Functions.convertToImage(history.getImage());
        image.setWidth("100%");
        showImage.setContent(image);
        addWindow(showImage);
    }

    private void buttonPrint() {
        Button print = new Button();
        print.addClickListener(click -> {
            data.removeStyleName("historyLabel");
            data.addStyleName("print");
            setContent(data);
            JavaScript.getCurrent().addFunction("repose", new JavaScriptFunction() {
                @Override
                public void call(JsonArray arguments) {
                    init(VaadinRequest.getCurrent());
                }
            });
            JavaScript.getCurrent().execute("print(); repose();");
        });
        print.setIcon(VaadinIcons.PRINT);
        print.setCaption("Imprimir");
        root.addComponent(print, "top: 54%; left:75%;");
    }
}
