package com.HistorialMedico.SuD.UI;

import com.HistorialMedico.SuD.Entities.History;
import com.HistorialMedico.SuD.Entities.SharedHistories;
import com.HistorialMedico.SuD.Entities.Users;
import com.HistorialMedico.SuD.Functions.Functions;
import com.HistorialMedico.SuD.Repositories.HistoryRepository;
import com.HistorialMedico.SuD.Repositories.SharedHistoriesRepository;
import com.HistorialMedico.SuD.Repositories.UsersRepository;
import com.HistorialMedico.SuD.Services.MailService;
import com.vaadin.annotations.Theme;
import com.vaadin.data.ValidationResult;
import com.vaadin.data.Validator;
import com.vaadin.data.ValueContext;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.Page;
import com.vaadin.server.UserError;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import com.vaadin.ui.Button;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;
import elemental.json.JsonArray;
import org.springframework.beans.factory.annotation.Autowired;


@SpringUI(path = "Histories/show")
@Theme("HistoriesTheme")
public class ShowHistory extends UI {

    @Autowired
    private HistoryRepository historyRepository;
    @Autowired
    private InsertWindow insertWindow;
    @Autowired
    private MyMenuBar myMenuBar;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private MailService mailService;
    @Autowired
    private SharedHistoriesRepository sharedHistoriesRepository;
    private AbsoluteLayout root;
    private History history;
    private RichTextArea data;
    private Boolean showButtons;
    private Grid<SharedHistories> users;

    @Override
    protected void init(VaadinRequest request) {
        setupLayout();
        addMenuBar();
        gettingHistory();
        addHeader();
        insertDate();
        contentHistory();
        comment();
        buttonShow();
        buttonPrint();
        buttonBack();
        if(getPage().getUriFragment().charAt(0)=='S'){
            if(showButtons){
                buttonEdit();
            }
            buttonRefresh();
        }
        else{
            buttonEdit();
            buttonShare();
            buttonViewShared();
            buttonDelete();
        }
    }

    private void setupLayout() {
        root = new AbsoluteLayout();
        root.setSizeFull();
        setContent(root);
    }

    private void addMenuBar() {
        root.addComponent(myMenuBar, "top:0%; left:0%");
    }

    private void gettingHistory() {
        if(getPage().getUriFragment().charAt(0)!='S'){
            Integer id = Integer.parseInt(getPage().getUriFragment());
            history = historyRepository.findById(id).get();
            showButtons = true;
        }
        else{
            String uri = getPage().getUriFragment().substring(1);
            Integer id = Integer.parseInt(uri);
            SharedHistories sharedHistories = sharedHistoriesRepository.findById(id).get();
            history = sharedHistories.getHistory();
            if(sharedHistories.getEdit()){
                showButtons = true;
            }
            else {
                showButtons = false;
            }
        }
    }

    private void addHeader() {
        VerticalLayout title = new VerticalLayout();
        title.setWidth("100%");
        title.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        Label header = new Label(history.getName());
        header.addStyleName(ValoTheme.LABEL_H1);
        title.addComponent(header);
        root.addComponent(title, "top:1%; left:0%;");

    }

    private  void comment(){
        TextField comment = new TextField("Comentario: ");
        comment.setWidth("70%");
        comment.setValue(history.getComment());
        comment.setReadOnly(true);
        root.addComponent(comment, "top: 90%; left:5%;");
    }

    private void insertDate() {
        Label date = new Label(history.getUserDate().toString());
        date.setCaption("Fecha:");
        root.addComponent(date, "top: 15%; left:5%;");
    }

    private void contentHistory() {
        data = new RichTextArea();
        data.setReadOnly(true);
        data.setValue(history.getHistory());
        data.setCaption("Historia");
        data.addStyleName("historyLabel");
        data.setSizeFull();
        root.addComponent(data, "top: 22%; left:5%");
    }

    private void buttonShow() {
        Button show = new Button();
        show.addClickListener(click -> {
            if(history.getImage()!=null){
                showWindow();
            }
            else{
                Notification.show("No se almaceno una imagen");
            }
        });
        show.setIcon(VaadinIcons.FILE_PICTURE);
        show.setCaption("Mostrar imagen");
        root.addComponent(show, "top: 22%; left:75%;");
    }

    private void showWindow(){
        final Window showImage = new Window("Imagen");
        showImage.setModal(true);
        showImage.center();
        showImage.setWidth("50%");
        Image image = Functions.convertToImage(history.getImage());
        image.setWidth("100%");
        showImage.setContent(image);
        addWindow(showImage);
    }

    private void buttonEdit() {
        Button Edit = new Button();
        Edit.addClickListener(click -> {
            insertWindow.center();
            insertWindow.initModify(history);
            insertWindow.setHeight("95%");
            insertWindow.setWidth("60%");
            insertWindow.setResizable(false);
            addWindow(insertWindow);
            insertWindow.addCloseListener(close -> {
                Page.getCurrent().reload();
            });
        });
        Edit.setIcon(VaadinIcons.EDIT);
        Edit.setCaption("Editar historia");
        root.addComponent(Edit, "top: 30%; left:75%;");
    }

    private void buttonPrint() {
        Button print = new Button();
        print.addClickListener(click -> {
            data.removeStyleName("historyLabel");
            data.addStyleName("print");
            setContent(data);
            JavaScript.getCurrent().addFunction("repose", new JavaScriptFunction() {
                @Override
                public void call(JsonArray arguments) {
                    init(VaadinRequest.getCurrent());
                }
            });
            JavaScript.getCurrent().execute("print(); repose();");
        });
        print.setIcon(VaadinIcons.PRINT);
        print.setCaption("Imprimir");
        root.addComponent(print, "top: 38%; left:75%;");
    }

    private void buttonShare() {
        Button share = new Button();
        share.addClickListener(click -> {
           Window shareWindow = new Window();
           shareWindow.center();
           shareWindow.setModal(true);
           shareWindow.setWidth("50%");
           shareWindow.setHeight("40%");
           FormLayout content = new FormLayout();
           content.setMargin(true);
           content.setSizeFull();
           Label title = new Label("Introduzca su email");
           title.setStyleName(ValoTheme.LABEL_H3);
           content.addComponent(title);
           TextField email = new TextField("E-mail");
           email.setSizeFull();
           email.addValueChangeListener(event -> {
               ValidationResult result = new EmailValidator(email.toString()).apply(event.getValue(), new ValueContext(email));
               if (result.isError()) {
                   UserError error = new UserError("No es un email");
                   email.setComponentError(error);
               } else {
                   email.setComponentError(null);
               }
           });
           content.addComponent(email);
           Button shareNow = new Button("Compartir");
           shareNow.setSizeFull();
           shareNow.addClickListener(event -> {
               Users user = usersRepository.findUsersByUserName(email.getValue());
              if(user!=null){
                  SharedHistories shared = new SharedHistories(user, history, false);
                  sharedHistoriesRepository.save(shared);
                  mailService.sharedAdvice(user);
                  shareWindow.close();
                  Notification.show("Se compartio la historia correctamente");
              }
              else{
                  mailService.sharedWAccount(email.getValue(), history.getId());
                  shareWindow.close();
                  Notification.show("Se envio un enlace al correo ingresado");
              }
           });
           content.addComponent(shareNow);
           addWindow(shareWindow);
           shareWindow.setContent(content);
        });
        share.setIcon(VaadinIcons.SHARE);
        share.setCaption("Compartir historia");
        root.addComponent(share, "top: 46%; left:75%;");
    }

    private void buttonViewShared() {
        Button viewShared = new Button();
        viewShared.addClickListener(click -> {
           Window people = new Window("A quien comparti");
           people.center();
           people.setModal(true);
           people.setHeight("50%");
           people.setWidth("50%");
           people.setClosable(true);
           VerticalLayout content = new VerticalLayout();
           content.setSizeFull();
           users = new Grid<>();
           users.setSizeFull();
           users.setItems(sharedHistoriesRepository.findSharedHistoriesByHistory(history));
           users.addColumn(sharedHistories -> sharedHistories.getUser().getFirstName() + " " + sharedHistories.getUser().getLastName()).setCaption("Nombre");
           users.addComponentColumn(sharedHistories -> buttons(sharedHistories)).setCaption("Puede editar");
           users.addComponentColumn(sharedHistories -> buttonSharedDelete(sharedHistories)).setCaption("Eliminar");
           content.addComponent(users);
           people.setContent(content);
           addWindow(people);
        });
        viewShared.setIcon(VaadinIcons.QUESTION);
        viewShared.setCaption("A quien comparti");
        root.addComponent(viewShared, "top: 54%; left:75%");
    }

    private Button buttonSharedDelete(SharedHistories sharedHistories) {
        Button deleteShared = new Button();
        deleteShared.setIcon(VaadinIcons.TRASH);
        deleteShared.addClickListener(click ->{
           sharedHistoriesRepository.delete(sharedHistories);
           users.setItems(sharedHistoriesRepository.findSharedHistoriesByHistory(history));
        });
        return deleteShared;
    }

    private HorizontalLayout buttons(SharedHistories sharedHistories) {
        HorizontalLayout buttonsContent = new HorizontalLayout();
        Button yes = new Button("SI");
        buttonsContent.addComponent(yes);
        Button no = new Button("NO");
        buttonsContent.addComponent(no);
        if(sharedHistories.getEdit()){
            yes.setEnabled(false);
        }
        else{
            no.setEnabled(false);
        }
        yes.addClickListener(click -> {
           sharedHistories.setEdit(true);
           sharedHistoriesRepository.save(sharedHistories);
           yes.setEnabled(false);
           no.setEnabled(true);
        });
        no.addClickListener(click -> {
            sharedHistories.setEdit(false);
            sharedHistoriesRepository.save(sharedHistories);
            no.setEnabled(false);
            yes.setEnabled(true);
        });
        return buttonsContent;
    }

    private void buttonBack() {
        Button back = new Button();
        back.addClickListener(click -> {
            Page.getCurrent().setLocation("/Histories");
        });
        back.setIcon(VaadinIcons.BACKSPACE);
        back.setCaption("Volver atras");
        root.addComponent(back, "top: 82%; left:75%;");
    }

    private void buttonDelete() {
        Button delete = new Button();
        delete.addClickListener(click -> {
            Window confirm = new Window("Estas seguro que quieres eliminar");
            confirm.setModal(true);
            confirm.setClosable(false);
            confirm.setResizable(false);
            confirm.setWidth("18%");
            confirm.setHeight("18%");
            HorizontalLayout buttons = new HorizontalLayout();
            buttons.setSizeFull();
            buttons.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
            buttons.setMargin(true);
            Button yes = new Button("SI");
            Button no = new Button("NO");
            buttons.addComponents(yes,no);
            confirm.setContent(buttons);
            yes.addClickListener(event -> {
                historyRepository.delete(history);
                Page.getCurrent().setLocation("/Histories");
                confirm.close();
            });
            no.addClickListener(event -> confirm.close());
            confirm.center();
            addWindow(confirm);
        });
        delete.setIcon(VaadinIcons.CLOSE);
        delete.setCaption("Eliminar historia");
        root.addComponent(delete, "top: 90%; left:75%;");
    }

    private void buttonRefresh() {
        Button refresh = new Button();
        refresh.addClickListener(click -> {
            getPage().reload();
        });
        refresh.setIcon(VaadinIcons.REFRESH);
        refresh.setCaption("Actualizar permisos");
        root.addComponent(refresh, "top:90%; left:75%;");
    }
}
