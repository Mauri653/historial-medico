package com.HistorialMedico.SuD.UI;

import com.HistorialMedico.SuD.Entities.Users;
import com.HistorialMedico.SuD.Repositories.UsersRepository;
import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringUI(path = "/recover")
@Theme("HistoriesTheme")
public class PasswordRecover extends UI {
    @Autowired
    private MyMenuBar myMenuBar;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    private VerticalLayout verticalLayout;
    private FormLayout formLayout;

    //coment
    @Override
    protected void init(VaadinRequest request) {
        setupLayout();
        setMyMenuBar();
        setFormLayout();
    }

    private void setupLayout() {
        verticalLayout = new VerticalLayout();
        verticalLayout.setMargin(false);
        verticalLayout.setSizeFull();
        setContent(verticalLayout);
    }
    private void setMyMenuBar(){
        verticalLayout.addComponent(myMenuBar);
    }

    private void setFormLayout() {
        verticalLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        formLayout = new FormLayout();
        formLayout.addStyleName("registerLayout");
        formLayout.setWidth("40%");
        formLayout.setMargin(true);
        addItems();
    }

    private void addItems() {

        Label header = new Label("Cambio de contraseña");
        header.addStyleName(ValoTheme.LABEL_H3);

        PasswordField password = new PasswordField("Nueva contraseña");
        password.setSizeFull();

        PasswordField Rpassword = new PasswordField("Confirmar Contraseña");
        Rpassword.setSizeFull();

        formLayout.addComponent(header);
        formLayout.addComponent(password);
        formLayout.addComponent(Rpassword);

        Button recoverButton = new Button("Guardar");
        recoverButton.setSizeFull();

        recoverButton.addClickListener(click -> {
            if (password.getValue().equals(Rpassword.getValue())) {
                Long id = Long.parseLong(getPage().getUriFragment());
                Users user = usersRepository.findById(id).get();
                user.setPassword(passwordEncoder.encode(password.getValue()));
                usersRepository.save(user);
                getPage().setLocation("/Login");
            }
            else{
                Notification.show("La contraseñas no son identicas");
            }
        });

        formLayout.addComponent(recoverButton);

        verticalLayout.addComponent(formLayout);
        verticalLayout.setExpandRatio(formLayout, 1.0f);
    }
}
