package com.HistorialMedico.SuD.Services;

import com.HistorialMedico.SuD.Entities.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class MailService {

    @Autowired
    private BCryptPasswordEncoder decoder;
    private JavaMailSender javaMailSender;

    @Autowired
    public MailService(JavaMailSender javaMailSender){
        this.javaMailSender=javaMailSender;
    }

    public void sendConfirmation(Users user){
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(user.getUsername());
        simpleMailMessage.setSubject("Confirmar correo");
        simpleMailMessage.setText("https://historial-medico.herokuapp.com/confirm#" + user.getId());
        javaMailSender.send(simpleMailMessage);
    }

    public void passwordHasForgotten(Users user){
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(user.getUsername());
        simpleMailMessage.setSubject("Recuperar password");
        simpleMailMessage.setText("Enlace de recuperacion: https://historial-medico.herokuapp.com/recover#" + user.getId());
        javaMailSender.send(simpleMailMessage);
    }

    public void sharedAdvice(Users user){
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(user.getUsername());
        simpleMailMessage.setSubject("Historia medica compartida");
        simpleMailMessage.setText("Le ha sido compartida historia medica revise sus historias compartidas: https://historial-medico.herokuapp.com/Histories");
        javaMailSender.send(simpleMailMessage);
    }

    public void sharedWAccount(String email, Integer id){
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(email);
        simpleMailMessage.setSubject("Historial medico online");
        simpleMailMessage.setText("Una historia medica le fue compartida: https://historial-medico.herokuapp.com/shared#" + id );
        javaMailSender.send(simpleMailMessage);
    }
}
